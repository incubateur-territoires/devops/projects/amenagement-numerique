data "scaleway_k8s_cluster" "prod" {
  provider   = scaleway.scaleway_production
  cluster_id = var.scaleway_cluster_production_cluster_id
}

# tflint-ignore: terraform_unused_declarations
data "scaleway_k8s_cluster" "dev" {
  provider   = scaleway.scaleway_development
  cluster_id = var.scaleway_cluster_development_cluster_id
}

data "gitlab_project" "extranet_back" {
  id = 42009787
}

data "gitlab_project" "extranet_front" {
  id = 42009849
}

data "gitlab_project" "hitpart" {
  id = 42009722
}

