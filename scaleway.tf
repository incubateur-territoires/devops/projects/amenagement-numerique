# Legacy buckets, in the wrong scaleway project

resource "scaleway_object_bucket" "backup_bucket_production" {
  provider = scaleway.scaleway_production
  name     = "${var.project_slug}-database-backups"
  region   = "fr-par"
}
resource "scaleway_object_bucket_acl" "backup_bucket_production" {
  provider = scaleway.scaleway_production
  bucket   = scaleway_object_bucket.backup_bucket_production.name
  acl      = "private"
}

resource "scaleway_object_bucket" "hitpart_backup_bucket_production" {
  provider = scaleway.scaleway_production
  name     = "${var.project_slug}-hitpart-database-backups"
  region   = "fr-par"
}
resource "scaleway_object_bucket_acl" "hitpart_backup_bucket_production" {
  provider = scaleway.scaleway_production
  bucket   = scaleway_object_bucket.hitpart_backup_bucket_production.name
  acl      = "private"
}
