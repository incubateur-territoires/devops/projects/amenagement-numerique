module "repo_config_extranet_backend" {
  source  = "gitlab.com/vigigloo/tf-modules-gitlab/projectcommonconfiguration"
  version = "0.0.1"

  project_id  = data.gitlab_project.extranet_back.id
  helm_values = <<-EOT
    resources:
      limits:
        memory: 512Mi
      requests:
        cpu: 100m

    ingress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: haproxy

    service:
      targetPort: 1338

    probes:
      liveness:
        tcpSocket:
          port: 1338
      readiness:
        tcpSocket:
          port: 1338
    persistence:
      uploads:
        mountPath: /app/documents
        size: 150Gi

    serviceAccount:
      create: false
  EOT
}

module "repo_config_extranet_frontend" {
  source  = "gitlab.com/vigigloo/tf-modules-gitlab/projectcommonconfiguration"
  version = "0.0.1"

  project_id  = data.gitlab_project.extranet_front.id
  helm_values = <<-EOT
    resources:
      limits:
        memory: 512Mi
      requests:
        cpu: 100m

    ingress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: haproxy

    serviceAccount:
      create: false
  EOT
}

module "repo_config_hitpart" {
  source  = "gitlab.com/vigigloo/tf-modules-gitlab/projectcommonconfiguration"
  version = "0.0.1"

  project_id  = data.gitlab_project.hitpart.id
  helm_values = <<-EOT
    frontend:
      envVars:
        RUNTIME_PROTOCOL: https
      ingress:
        enabled: false
        annotations:
          kubernetes.io/ingress.class: haproxy
      resources:
        limits:
          memory: 512Mi
        requests:
          cpu: 100m
      probes:
        liveness:
          initialDelaySeconds: 0
        readiness:
          initialDelaySeconds: 0
      serviceAccount:
        create: false

    backend:
      envVars:
        RUNTIME_DB_HOST:
          secretKeyRef:
            name: postgresql-hitpart-pguser-postgresql-hitpart
            key: host
        RUNTIME_DB_PORT:
          secretKeyRef:
            name: postgresql-hitpart-pguser-postgresql-hitpart
            key: port
        RUNTIME_DB_NAME:
          secretKeyRef:
            name: postgresql-hitpart-pguser-postgresql-hitpart
            key: dbname
        RUNTIME_DB_USER:
          secretKeyRef:
            name: postgresql-hitpart-pguser-postgresql-hitpart
            key: user
        RUNTIME_DB_PASS:
          secretKeyRef:
            name: postgresql-hitpart-pguser-postgresql-hitpart
            key: password
      probes:
        liveness:
          tcpSocket:
            port: 80
          initialDelaySeconds: 0
        readiness:
          tcpSocket:
            port: 80
          initialDelaySeconds: 0
      ingress:
        enabled: false
        annotations:
          kubernetes.io/ingress.class: haproxy
      resources:
        limits:
          memory: 512Mi
        requests:
          cpu: 100m
      persistence:
        uploads:
          mountPath: /app/web/uploads
          size: 150Gi
      serviceAccount:
        create: false
  EOT
}
