# tflint-ignore: terraform_unused_declarations
variable "scaleway_organization_id" {
  type = string
}

variable "prod_francethd_domain" {
  type = string
}

variable "prod_agencenumerique_domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "scaleway_cluster_development_project_id" {
  type = string
}
variable "scaleway_cluster_development_access_key" {
  type = string
}
variable "scaleway_cluster_development_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_cluster_development_cluster_id" {
  type = string
}
variable "scaleway_cluster_production_project_id" {
  type = string
}
variable "scaleway_cluster_production_access_key" {
  type = string
}
variable "scaleway_cluster_production_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_cluster_production_cluster_id" {
  type = string
}

variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}

variable "gitlab_token" {
  type = string
}

variable "grafana_development_config" {
  type = object({
    url            = string
    api_key        = string
    org_id         = string
    loki_url       = string
    prometheus_url = string
  })
  sensitive = true
}

variable "grafana_production_config" {
  type = object({
    url            = string
    api_key        = string
    org_id         = string
    loki_url       = string
    prometheus_url = string
  })
  sensitive = true
}

variable "smtp_port" {
  type = string
}
variable "smtp_host" {
  type = string
}
variable "smtp_user" {
  type      = string
  sensitive = true
}
variable "production_smtp_password" {
  type      = string
  sensitive = true
}

variable "production_hitpart_client_id" {
  type      = string
  sensitive = true
}
variable "production_hitpart_client_secret" {
  type      = string
  sensitive = true
}

variable "production_snapshot_extranet_volume" {
  type        = string
  default     = null
  description = "VolumeHandle for the PersistentVolume that contains the uploads for extranet."
  sensitive   = true
}
variable "production_snapshot_hitpart_volume" {
  type        = string
  default     = null
  description = "VolumeHandle for the PersistentVolume that contains the uploads for hitpart."
  sensitive   = true
}
