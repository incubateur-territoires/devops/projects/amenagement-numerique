module "configure-repository-for-deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.1"

  repositories = var.repositories
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)
}

resource "gitlab_project_variable" "base_domain_backend_extranet" {
  project           = var.extranet_backend_project_id
  protected         = false
  environment_scope = var.gitlab_environment_scope

  key   = "BASE_DOMAIN"
  value = "extranet-api.${var.francethd_domain}"
}

resource "gitlab_project_variable" "base_domain_frontend_extranet" {
  project           = var.extranet_frontend_project_id
  protected         = false
  environment_scope = var.gitlab_environment_scope

  key   = "BASE_DOMAIN"
  value = "extranet.${var.francethd_domain}"
}

resource "gitlab_project_variable" "extranet_back_api_url" {
  project           = var.extranet_frontend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "DOCKER_BUILD_ARG_API_DOMAIN"
  value = gitlab_project_variable.base_domain_backend_extranet.value
}

resource "random_password" "jwt_secret" {
  length = 256
}
resource "gitlab_project_variable" "extranet_jwt_secret" {
  project           = var.extranet_backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_JWT_SECRET"
  value = random_password.jwt_secret.result
}

resource "gitlab_project_variable" "base_domain_hitpart" {
  project           = var.hitpart_project_id
  protected         = false
  environment_scope = var.gitlab_environment_scope

  key   = "BASE_DOMAIN"
  value = "old.francemobile.${var.agencenumerique_domain}"
}

resource "random_password" "hitpart_backend_secret" {
  length           = 40
  min_special      = 40
  override_special = "01234567890abcdef"
}

resource "gitlab_project_variable" "hitpart_secret" {
  project           = var.hitpart_project_id
  protected         = false
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_BACKEND_ENV_VAR_RUNTIME_SECRET"
  value = random_password.hitpart_backend_secret.result
}

resource "gitlab_project_variable" "hitpart_mailer_host" {
  project           = var.hitpart_project_id
  protected         = false
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_BACKEND_ENV_VAR_RUNTIME_MAILER_HOST"
  value = var.smtp_host
}

resource "gitlab_project_variable" "hitpart_mailer_port" {
  project           = var.hitpart_project_id
  protected         = false
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_BACKEND_ENV_VAR_RUNTIME_MAILER_PORT"
  value = var.smtp_port
}

resource "gitlab_project_variable" "hitpart_mailer_user" {
  project           = var.hitpart_project_id
  protected         = false
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_BACKEND_ENV_VAR_RUNTIME_MAILER_USER"
  value = var.smtp_user
}

resource "gitlab_project_variable" "hitpart_mailer_password" {
  project           = var.hitpart_project_id
  protected         = false
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_BACKEND_ENV_VAR_RUNTIME_MAILER_PASSWORD"
  value = var.smtp_password
}

resource "gitlab_project_variable" "hitpart_client_id" {
  project           = var.hitpart_project_id
  protected         = false
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_FRONTEND_ENV_VAR_RUNTIME_CLIENT_ID"
  value = var.hitpart_client_id
}

resource "gitlab_project_variable" "hitpart_client_secret" {
  project           = var.hitpart_project_id
  protected         = false
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_FRONTEND_ENV_VAR_RUNTIME_CLIENT_SECRET"
  value = var.hitpart_client_secret
}
