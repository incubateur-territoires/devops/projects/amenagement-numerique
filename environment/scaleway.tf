# Buckets containing files that were backed up when migrating from azure in december 2022.
# These buckets are otherwise not used by the applications

resource "scaleway_object_bucket" "extranet_migration_backup" {
  provider = scaleway.project
  name     = "${var.project_slug}-extranet-migration-dump"
}
resource "scaleway_object_bucket" "hitpart_migration_backup" {
  provider = scaleway.project
  name     = "${var.project_slug}-hitpart-migration-dump"
}
