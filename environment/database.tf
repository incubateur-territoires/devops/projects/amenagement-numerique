locals {
  db_replicas = contains(["development", "production"], var.gitlab_environment_scope) ? 2 : 1
  db_backups  = contains(["development", "production"], var.gitlab_environment_scope)
}

resource "scaleway_object_bucket" "extranet_db_backups" {
  provider = scaleway.project
  name     = "${var.project_slug}-extranet-db-backups"
}

module "postgresql_extranet" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.22"

  chart_name = "postgresql"
  namespace  = module.namespace.namespace
  values = [
    <<-EOT
    postgresVersion: 14
    imagePostgres: null
    imagePgBackRest: null
    EOT
    , file("${path.module}/db_resources.yaml")
  ]
  pg_replicas = local.db_replicas

  pg_volume_size = "40Gi"

  pg_backups_volume_enabled             = local.db_backups ? true : false
  pg_backups_volume_size                = local.db_backups ? "30Gi" : null
  pg_backups_volume_full_schedule       = local.db_backups ? "0 2 * * 0" : null
  pg_backups_volume_incr_schedule       = local.db_backups ? "0 2 * * 1-6" : null
  pg_backups_volume_full_retention      = local.db_backups ? 4 : null
  pg_backups_volume_full_retention_type = local.db_backups ? "count" : null

  pg_backups_s3_enabled       = local.db_backups ? true : false
  pg_backups_s3_bucket        = local.db_backups ? scaleway_object_bucket.extranet_db_backups.name : null
  pg_backups_s3_region        = local.db_backups ? "fr-par" : null
  pg_backups_s3_endpoint      = local.db_backups ? "s3.fr-par.scw.cloud" : null
  pg_backups_s3_access_key    = local.db_backups ? var.scaleway_project_config.access_key : null
  pg_backups_s3_secret_key    = local.db_backups ? var.scaleway_project_config.secret_key : null
  pg_backups_s3_full_schedule = local.db_backups ? "0 3 * * 0" : null
  pg_backups_s3_incr_schedule = local.db_backups ? "0 3 * * 1-6" : null
}

resource "gitlab_project_variable" "db-host" {
  project           = var.extranet_backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DB_URI"
  value = "${module.postgresql_extranet.uri}?sslmode=no-verify"
}

resource "scaleway_object_bucket" "hitpart_db_backups" {
  provider = scaleway.project
  name     = "${var.project_slug}-hitpart-db-backups"
}

module "postgresql_hitpart" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.22"

  chart_name = "postgresql-hitpart"
  namespace  = module.namespace.namespace
  values = [
    <<-EOT
    imagePostgres: null
    imagePgBackRest: null
    postgresVersion: 14
    postGISVersion: 3.2
    EOT
    , file("${path.module}/db_resources.yaml")
  ]
  pg_replicas = 1

  pg_volume_size = "10Gi"

  pg_backups_volume_enabled             = local.db_backups ? true : false
  pg_backups_volume_size                = local.db_backups ? "30Gi" : null
  pg_backups_volume_full_schedule       = local.db_backups ? "35 2 * * 0" : null
  pg_backups_volume_incr_schedule       = local.db_backups ? "35 2 * * 1-6" : null
  pg_backups_volume_full_retention      = local.db_backups ? 4 : null
  pg_backups_volume_full_retention_type = local.db_backups ? "count" : null

  pg_backups_s3_enabled       = local.db_backups ? true : false
  pg_backups_s3_bucket        = local.db_backups ? scaleway_object_bucket.hitpart_db_backups.name : null
  pg_backups_s3_region        = local.db_backups ? "fr-par" : null
  pg_backups_s3_endpoint      = local.db_backups ? "s3.fr-par.scw.cloud" : null
  pg_backups_s3_access_key    = local.db_backups ? var.scaleway_project_config.access_key : null
  pg_backups_s3_secret_key    = local.db_backups ? var.scaleway_project_config.secret_key : null
  pg_backups_s3_full_schedule = local.db_backups ? "35 4 * * 0" : null
  pg_backups_s3_incr_schedule = local.db_backups ? "35 4 * * 1-6" : null
}
