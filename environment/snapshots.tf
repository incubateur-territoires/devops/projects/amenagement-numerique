resource "kubernetes_cron_job_v1" "hitpart_snapshot" {
  count = var.snapshot_hitpart_volume == null ? 0 : 1
  metadata {
    name      = "hitpart-snapshot"
    namespace = module.namespace.namespace
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 10
    schedule                      = "50 3 * * SAT"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10

    job_template {
      metadata {
        annotations = {}
        labels      = {}
      }

      spec {
        backoff_limit = 3

        template {
          metadata {
            annotations = {}
            labels      = {}
          }
          spec {
            container {
              name  = "scaleway-snapshot"
              image = "registry.gitlab.com/vigigloo/tools/scaleway-pvc-snapshot:0.2.1-scw-2.5.1"

              resources {
                limits = {
                  cpu    = "500m"
                  memory = "512Mi"
                }
              }

              env {
                name  = "SCW_ACCESS_KEY"
                value = var.scaleway_access_key
              }
              env {
                name  = "SCW_SECRET_KEY"
                value = var.scaleway_secret_key
              }
              env {
                name  = "SCW_DEFAULT_ORGANIZATION_ID"
                value = var.scaleway_organization_id
              }
              env {
                name  = "SCW_DEFAULT_PROJECT_ID"
                value = var.scaleway_project_id
              }
              env {
                name  = "SCW_DEFAULT_ZONE"
                value = "fr-par-1"
              }
              env {
                name  = "VOLUME_HANDLE"
                value = var.snapshot_hitpart_volume
              }
              env {
                name  = "SNAPSHOT_NAME"
                value = "hitpart-uploads"
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_cron_job_v1" "extranet_snapshot" {
  count = var.snapshot_extranet_volume == null ? 0 : 1
  metadata {
    name      = "extranet-snapshot"
    namespace = module.namespace.namespace
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 10
    schedule                      = "50 3 * * SAT"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10

    job_template {
      metadata {
        annotations = {}
        labels      = {}
      }

      spec {
        backoff_limit = 3

        template {
          metadata {
            annotations = {}
            labels      = {}
          }
          spec {
            container {
              name  = "scaleway-snapshot"
              image = "registry.gitlab.com/vigigloo/tools/scaleway-pvc-snapshot:0.2.1-scw-2.5.1"

              resources {
                limits = {
                  cpu    = "500m"
                  memory = "512Mi"
                }
              }

              env {
                name  = "SCW_ACCESS_KEY"
                value = var.scaleway_access_key
              }
              env {
                name  = "SCW_SECRET_KEY"
                value = var.scaleway_secret_key
              }
              env {
                name  = "SCW_DEFAULT_ORGANIZATION_ID"
                value = var.scaleway_organization_id
              }
              env {
                name  = "SCW_DEFAULT_PROJECT_ID"
                value = var.scaleway_project_id
              }
              env {
                name  = "SCW_DEFAULT_ZONE"
                value = "fr-par-1"
              }
              env {
                name  = "VOLUME_HANDLE"
                value = var.snapshot_extranet_volume
              }
              env {
                name  = "SNAPSHOT_NAME"
                value = "extranet-uploads"
              }
            }
          }
        }
      }
    }
  }
}
