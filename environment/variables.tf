variable "francethd_domain" {
  type = string
}

variable "agencenumerique_domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}

variable "namespace" {
  type = string
}

variable "repositories" {
  type = list(number)
}

variable "namespace_quota_max_cpu" {
  type    = number
  default = 2
}

variable "namespace_quota_max_memory" {
  type    = string
  default = "12Gi"
}

variable "monitoring_org_id" {
  type = string
}

variable "extranet_backend_project_id" {
  type = number
}

variable "extranet_frontend_project_id" {
  type = number
}

variable "hitpart_project_id" {
  type = number
}

variable "scaleway_organization_id" {
  type = string
}
variable "scaleway_project_id" {
  type = string
}
variable "scaleway_access_key" {
  type = string
}
variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}

variable "backup_bucket_name" {
  type = string
}
variable "hitpart_backup_bucket_name" {
  type = string
}

variable "smtp_port" {
  type = string
}
variable "smtp_host" {
  type = string
}
variable "smtp_user" {
  type      = string
  sensitive = true
}
variable "smtp_password" {
  type      = string
  sensitive = true
}

variable "hitpart_client_id" {
  type      = string
  sensitive = true
}
variable "hitpart_client_secret" {
  type      = string
  sensitive = true
}

variable "snapshot_extranet_volume" {
  type        = string
  default     = null
  description = "Name of the PersistentVolume that contains the uploads for extranet."
  sensitive   = true
}
variable "snapshot_hitpart_volume" {
  type        = string
  default     = null
  description = "Name of the PersistentVolume that contains the uploads for hitpart."
  sensitive   = true
}

variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}
