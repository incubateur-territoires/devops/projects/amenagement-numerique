locals {
  old_domain = "francemobile.agencedunumerique.gouv.fr"
  new_domain = "tous-connectes.anct.gouv.fr"
}
module "redirection" {
  source  = "gitlab.com/vigigloo/tools-k8s/nginxredirect"
  version = "0.1.0"

  chart_name    = "redirection"
  chart_version = "0.1.0"
  namespace     = module.namespace.namespace

  values = [
    <<-EOT
    ingress:
      className: null
      enabled: false
      certManagerClusterIssuer: letsencrypt-prod
      host: ${local.old_domain}
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
    EOT
  ]
  redirect_from = local.old_domain
  redirect_to   = "https://${local.new_domain}"

  requests_cpu    = "10m"
  requests_memory = null
  limits_cpu      = null
  limits_memory   = "20Mi"
}
