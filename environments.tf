locals {
  repositories = [data.gitlab_project.extranet_back.id, data.gitlab_project.extranet_front.id, data.gitlab_project.hitpart.id]
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  agencenumerique_domain   = var.prod_agencenumerique_domain
  francethd_domain         = var.prod_francethd_domain
  namespace                = var.project_slug
  repositories             = local.repositories
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 16
  namespace_quota_max_memory = "24Gi"

  monitoring_org_id = random_string.production_secret_org_id.result

  backup_bucket_name         = scaleway_object_bucket.backup_bucket_production.name
  hitpart_backup_bucket_name = scaleway_object_bucket.hitpart_backup_bucket_production.name

  scaleway_organization_id = var.scaleway_organization_id
  scaleway_project_id      = var.scaleway_cluster_production_project_id
  scaleway_access_key      = var.scaleway_cluster_production_access_key
  scaleway_secret_key      = var.scaleway_cluster_production_secret_key

  extranet_backend_project_id  = data.gitlab_project.extranet_back.id
  extranet_frontend_project_id = data.gitlab_project.extranet_front.id
  hitpart_project_id           = data.gitlab_project.hitpart.id

  smtp_host     = var.smtp_host
  smtp_port     = var.smtp_port
  smtp_user     = var.smtp_user
  smtp_password = var.production_smtp_password

  hitpart_client_id     = var.production_hitpart_client_id
  hitpart_client_secret = var.production_hitpart_client_secret

  snapshot_extranet_volume = var.production_snapshot_extranet_volume
  snapshot_hitpart_volume  = var.production_snapshot_hitpart_volume

  scaleway_project_config = var.scaleway_project_config
  providers = {
    scaleway.project = scaleway.scaleway_project
  }
}
